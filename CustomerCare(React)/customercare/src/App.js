import Hom from './hom.js'
import Frontend from './Frontend';
import Backend from './Backend';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes,

} from "react-router-dom";

function App() {
  return (
    <>
    <div>
      <Router>
          <Routes>
          <Route exact path="/" element={<Hom />} /> 
          <Route exact path="/frontend" element={<Frontend/>}/> 
          <Route exact path="/backend" element={<Backend/>}/>
          </Routes>
      </Router>
    </div>
    </>
  );
}

export default App;
