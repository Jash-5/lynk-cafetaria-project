import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import './Forall.css'
function Frontend() {
    let history = useNavigate()
    const [data, setData] = useState([])
    async function handleClick() {
        fetch('http://localhost:8000/orders')
            .then(response => response.json())
            .then(data => {
                if (data.present === true) {
                    setData(data.dat)
                }
                else {
                    setData([])
                }
            });

    }
    function front() {
        history('/Backend')
    }
    function Del(item) {
        var data1 = {
            'issue': item,
            'type': 'frontend'
        }
        fetch("http://localhost:8000/del", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'

            },
            body: JSON.stringify(data1)
        }).then(function (response) {
            handleClick()

        })
    }
    return (
        <>
            <div className='Alldiv'>
                <button type="submit" onClick={handleClick}>View Issues</button>
            </div>
            <div className='Alldiv'>

                <table>
                    <thead>
                        <tr>
                            <th>
                                Issues
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.map(item => {
                                return <tr key={Math.random()}><td>{item.issue}</td><td><button type="button" onClick={Del.bind(this, item.issue)}>Solved</button></td></tr>
                            })
                        }
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <div className='Alldiv'>
                <button onClick={front}>Go To Backend</button>
            </div>
        </>
    )
}



export default Frontend